# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 13:53:29 2020

@author: Alex Watson

Basic machine learning algorithm code for determining cracks on a rock face
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import gaussian_filter
from scipy.signal import convolve2d
# Import datasets, classifiers and performance metrics
#from sklearn import datasets, svm, metrics
from sklearn.model_selection import train_test_split
from sklearn.metrics import auc
import time
import multiprocessing
# Import Image importing functions
#from PIL import Image


#===============================Functions======================================

def subsample(x, y, sz, w, h, ntot):
    n1 = w/sz
    n1 = int(n1)
    n2 = h/sz
    n2 = int(n2)
    #n_f = n*ntot
    #n_f = int(n_f)
    #bases = np.zeros([n_f,sz,sz,3])
    #traces = np.zeros([n_f,sz,sz])
    base_temp = np.zeros([ntot,sz,sz,3])
    trace_temp = np.zeros([ntot,sz,sz])
    img = np.zeros([n2,sz,sz,3])
    img2 = np.zeros([n2,sz,sz])
    #for k in np.arange(n):
    for i in np.arange(n1):
        for j in np.arange(n2):
            img[j] = x[sz*j:sz*j+sz,sz*i:sz*i+sz]
            img2[j] = y[sz*j:sz*j+sz,sz*i:sz*i+sz]
        base_temp[n2*i:n2*i+n2] = img
        trace_temp[n2*i:n2*i+n2] = img2
        #bases[ntot*k:ntot*k+ntot] = base_temp
        #traces[ntot*k:ntot*k+ntot]= trace_temp
    return base_temp,trace_temp

    

def loadimgdata(dPar, i):
    """
    function for loading the image data into python and combining it into a single list
    Params:
        dPar: dictionary of parameters (# images, image width, image height)
    	i: the image (should be numbered 0-(dPar['n']-1)
        """
    image = plt.imread("Data/Base/%i.JPG"%i)
    trace = plt.imread("Data/Trace/%i.jpg"%i)
    base, trace = subsample(image, trace, dPar['sz'], dPar['w'], dPar['h'], dPar['ntot'])

    for i in range(dPar['ntot']):
        trace[i] = np.logical_and(trace[i] <=2, trace[i] >= 0)
        trace[i] = gaussian_filter(trace[i], sigma=3)
    return base, trace


def threshold_model(x, sobel_x, x_a, threshold1, threshold2, threshold3):
    """
    function that tries to label cracks based off of a simple logical
    Params:
        x: 3d array of a single base image (width, height, RGB)
	x_a: the average value of x over a 10x10 pixel area (grayscale)
	x_c: x sobel-filtered
        threshold1: number from 0 to 255, color value/intensity of a single pixel of an image
	threshold2: value/intensity of sobel edge detection
	threshold3: averaged intensity value
    """
    predict1 = np.logical_and(x[:,:,0] <=threshold1, x[:,:,1] <=threshold1, x[:,:,2] <=threshold1)
    predict2 = x_a<=threshold3
    predict3 = sobel_x>=threshold2
    predict = (predict1+predict2+predict3)/3
    predict = gaussian_filter(predict, sigma = 1)
    return predict


# challenge -> find best threshold automaticaly
def train_model(x,y,ntot,n,sz,pool_size):
    """
    function that trains the threshold model using the training data by altering the threshold
    Parameters:
        x: training base images
        y: crack traces (labels) pertaining to the training images x
        ntot: # of subsampled images from each base image
	n: # of base images
	sz: dimensions of subsamples images (sz x sz)
	pool_size: # of CPU cores used for multiprocessing
    """
    bestThreshold1 = 255
    bestThreshold2 = 5
    bestThreshold3 = 100
    ntrain = ntot*n/2
    ntrain = int(ntrain)
    for i in np.arange(10):
        with multiprocessing.Pool(processes=pool_size) as pool:
            x_c = pool.map(sobel, [(x[j]) for j in range(ntrain)])
            x_avg = pool.map(avg_conv, [(x[j]) for j in range(ntrain)])
            predict = pool.starmap(threshold_model, [(x[j],x_c[j],x_avg[j],bestThreshold1,bestThreshold2,bestThreshold3) for j in range(ntrain)])
            pool.close()
        predict = np.array(predict)
        if np.sum(predict) >= np.sum(y):
            bestThreshold1 = bestThreshold1/(2+(1/(i+1)))
            bestThreshold3 = bestThreshold3/(2+(1/(i+1)))
            bestThreshold2 = bestThreshold2 + (1/(i+2))*bestThreshold2
        else:
            bestThreshold1 = bestThreshold1 + (1/(i+2))*bestThreshold1
            bestThreshold3 = bestThreshold3 + (1/(i+2))*bestThreshold3
            bestThreshold2 = bestThreshold2/(2+(1/(i+1)))
    
    thresh_list = [bestThreshold1,bestThreshold2,bestThreshold3]        
    return thresh_list
    

def test_model(x,y,thresh_list,n,ntot,sz,use_weights,pool_size):
    """
    function that tests the trained model against the test data, and computes the error of the predictions
    Parameters:
        x: testing/validation base images
        y: crack traces (labels) pertaining to the images x
	thresh_list: list of trained model thresholds (color, edge, avg)
        n: total # of images
	ntot: # of subsampled images from each base image
        sz: dimensions of subsampled images (sz x sz)
        use_weights: if 'True', then will calculate different weights for noncracks and cracks
	pool_size: # of CPU cores used in multiprocessing
    """
    ntest = 0.3*ntot*n
    ntest = int(ntest)
    cost = np.zeros([ntest, sz, sz])
    ni = np.zeros(ntest)
    n0 = np.zeros(ntest)
    N  = sz*sz
    w = np.ones(y.shape)
    for i in range(ntest):
        ni[i] = np.sum(y[i]) + 1**-5
        n0[i] = N-ni[i]
    if use_weights == 'True':
        w1 = np.zeros(ntest)
        w2 = np.zeros(ntest)
        for i in range(ntest):
            w1[i] =((ni[i]/N)**-1)
            w2[i] = ((n0[i]/N)**-1)
            w[i][y[i]==1] = w1[i]
            w[i][y[i]==0] = w2[i]
    else:
        for i in range(ntest):
            w[i] = (ni[i]/N)**-1
    with multiprocessing.Pool(processes=pool_size) as pool:      
        x_c = pool.map(sobel, [(x[k]) for k in range(ntest)])
        x_a = pool.map(avg_conv, [(x[k]) for k in range(ntest)])
        predict = pool.starmap(threshold_model, [(x[k],x_c[k],x_a[k],thresh_list[0],thresh_list[1],thresh_list[2]) for k in range(ntest)])
        pool.close()
    predict = np.array(predict)
    for k in range(ntest):
        cost[k] = (predict[k]-y[k])*w[k]
    total_cost = np.sum(cost)
    return total_cost, predict


def confmatrix(predict,y, thresh):
    """
    function that creates a simple confusion matrix of tested predictions versus the actual crack traces,
    and also computes the percent of guesses correct
    Parameters:
        predict: tested predictions from the test_model function
        y: the crack traces (labels) pertaining to the testing/validation images
	thresh: threshold value to be counted as a predicted crack
    """
    pospos = (np.sum(np.logical_and(predict>=thresh,y>0.15)))
    negneg = (np.sum(np.logical_and(predict<thresh,y<=0.15)))
    posneg = (np.sum(np.logical_and(predict>=thresh,y<=0.15)))
    negpos = (np.sum(np.logical_and(predict<thresh,y>0.15)))
    
    confmatrix = np.array([[pospos/(pospos+negpos),posneg/(posneg+negneg)],
                           [negpos/(pospos+negpos),negneg/(posneg+negneg)]])
    return confmatrix

def sobel(x):
    s_filter = np.array([[-1,0,1],
                         [-2,0,2],
                         [-1,0,1]])
    x_grey = (x[:,:,0]+x[:,:,1]+x[:,:,2])/3
    x_x = convolve2d(x_grey, s_filter)
    x_y = convolve2d(x_grey, np.flip(s_filter.T, axis=0))
    x_sobel = np.sqrt(x_x**2 + x_y**2)
    x_sobel *= 255/ x_sobel.max()
    x_sobel = x_sobel[0:250, 0:250]
    return x_sobel    

def avg_conv(x):
    avg_filter = np.ones([10,10])
    x_grey = (x[:,:,0]+x[:,:,1]+x[:,:,2])/3
    x_avg = convolve2d(x_grey, avg_filter)
    x_avg *= 255/x_avg.max()
    x_avg = x_avg[0:250, 0:250]
    return x_avg
    
def roc(predict, y):
    nit=100
    tpr = np.zeros(nit)
    fpr = np.zeros(nit)
    thresh = 0   
    for i in np.arange(nit-1):
        tpr[i] = np.sum(np.logical_and(predict>=thresh,y>0.15))/np.sum(y>0.15)
        fpr[i] = np.sum(np.logical_and(predict>=thresh,y<=0.15))/np.sum(y<=0.15)
        thresh = thresh+(i*1/nit)
    area = auc(fpr,tpr)
    return tpr, fpr, area
#================================Parameters====================================

dPar = { 'n'   : 10,    # number of total photos
         'h'   : 4000,  # pixel height of photos
         'w'   : 6000,  # pixel width of photos
         'sz'  : 250,
         'ntot': 384,
        }

vali_thresh = 0.05
pool_size = 32
#===========================Run the Functions==================================


if __name__ == '__main__':
    t1 = time.perf_counter()

    # load the images and subsample, preprocess them
    images = []
    traces = []
    with multiprocessing.Pool(processes=pool_size) as pool:
        for image, trace in pool.starmap(loadimgdata, [(dPar, i) for i in range(dPar['n'])]):
            for i in range(dPar['ntot']):
                images.append(image[i])
                traces.append(trace[i])
        pool.close()
    images = np.array(images)
    traces = np.array(traces)        
    
    # split the subsampled images into training, testing and validation sets
    X_train, X_test, y_train, y_test = train_test_split(
        images, traces, test_size=0.5, shuffle=False)

    X_vali, X_test, y_vali, y_test = train_test_split(
        X_test, y_test, test_size=0.4, shuffle=False)

    # train threshold model on training dataset (returns trained thresholds to use with threshold_model) 
    t_list = train_model(X_train, y_train, dPar['ntot'], dPar['n'], dPar['sz'], pool_size)
    
    # tests trained threshold model on validation dataset (returns total cost and model predictions     
    t_cost, predict = test_model(X_vali, y_vali, t_list, dPar['n'], dPar['ntot'], dPar['sz'], 'True', pool_size)

    # create a confusion matrix that shows % of true/false positives/negatives
    cm = confmatrix(predict, y_vali, vali_thresh)
    print(np.sum(y_vali>0)/(np.sum(y_vali>0)+np.sum(y_vali==0)))

    # creates roc curve of results
    tpr, fpr, roc_auc = roc(predict, y_vali)

    # plot roc curve
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

    # plot trace vs. prediction example
    # choose subsample image to plot
    tileNum = 665;
    #plot
    x_vali_grey = (X_vali[tileNum,:,:,0]+X_vali[tileNum,:,:,1]+X_vali[tileNum,:,:,2])/3    
    plt.subplot(1,3,1)
    plt.imshow(x_vali_grey, cmap='gray')
    plt.subplot(1,3,2)    
    plt.imshow(predict[tileNum] >vali_thresh, cmap='gray')
    plt.subplot(1,3,3)
    plt.imshow(y_vali[tileNum]>0.15, cmap='gray')

    t2 = time.perf_counter()
    print(t2-t1) # total run time
